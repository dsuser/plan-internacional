import { StyleSheet, Platform } from 'react-native';
const styles = StyleSheet.create({
  // Estilos para menus desplegables

  header: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    padding: 5,
    borderColor: '#ffffff',
    borderStyle: 'dashed',
    borderWidth: 0.5
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
    color:'#ffffff',
   
  },
  content: {
    padding: 14,
    paddingTop: 10,
  },
  // Fin Estilos para menus desplegables

  MainContainer: {
    alignItems: 'center',
     paddingTop: 10,
     backgroundColor: 'rgba(0,0,0,0.4)',
     paddingBottom :10,
      width: 350,
      height: 400
  },
MainContainerbuton: {
    
    flexDirection: 'row',
      justifyContent: 'flex-end',
     paddingTop: 10,
     paddingRight :20,
     paddingBottom :10,
      width: 350,
      
  },
  MainContainerlink: {
    
    flexDirection: 'row',
      justifyContent: 'center',
     paddingTop: 30,
     paddingLeft :25,
     paddingBottom :15,
      width: 350,
      
      
  },
  MainContainerlogitex: {
    
    flexDirection: 'row',
    justifyContent: 'flex-start',
     
    paddingLeft :25,
    paddingBottom :10,
    width: 350,
      
      
  },
  sideMenuContainer: {

    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 20
  },

  sideMenuProfileIcon:
  {
    resizeMode: 'center',
    width: 150, 
    height: 150, 
    borderRadius: 150/2
  },

  sideMenuIcon:
  {
    resizeMode: 'center',
    width: 28, 
    height: 28, 
    marginRight: 10,
    marginLeft: 20
    
  },

  menuText:{
    fontSize: 15,
    color: '#222222',
    
  },container : {
        flex: 5,
    alignItems: 'center',
     paddingTop: 250,
     



  },
imagencontain:{

  flex: 1,
   
        resizeMode: 'cover'
},
  inputBox: {
    width:300,
    height :40 ,
    backgroundColor:'rgb(255, 255,255)',
    borderRadius: 10,
    paddingHorizontal:16,
    fontSize:16,
    color:'#0F0B0A',
    marginVertical: 10,
     borderColor: '#009688',
    borderWidth: 1
  },
  buttons: {
    width:100,
    backgroundColor:'rgb(240, 195,0)',
     borderRadius: 15,
      marginVertical: 5,
      paddingVertical: 5,
      
      

  },

  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  LoginText: {
    fontSize:26,
    fontWeight:'100',
    color:'#ffffff',
    textAlign:'right' 

  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:15,
      borderBottomWidth: 1,
      width:300,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      justifyContent: 'flex-end'
      ,marginTop :30
  },
   inputContainer2: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:15,
      borderBottomWidth: 1,
      width:300,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
      
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center',
    
  },
  buttonContainer: {
    height:40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:100,
    borderRadius:10,
  },
  loginButton: {
    backgroundColor: 'rgb(240, 195,0)',
  },
  loginText: {
    color: 'black',
    fontSize: 18,
  },
  check:
  {
    flexDirection: 'row',
      justifyContent: 'flex-start',
      paddingTop: 5,
     paddingRight :20,
     paddingLeft:25,
     paddingBottom :10,
      width: 350,
     
    color:'white'
  },checkText: {
    color: 'white',
    fontSize: 18,
    fontWeight:'400',
    paddingRight:50,
    marginLeft:16,
  }
  ,passText: {
    color: 'white',
    fontSize: 18,
    fontWeight:'400',
    paddingRight:50,
    marginLeft:16,
    textDecorationLine: 'underline'
  },
  hamburger:
  {
    marginLeft:80,
    width:'100%',
    flexDirection: 'row',
      justifyContent: 'flex-start',
  },
  bod:{
    marginTop:150
  },
   con:{
    marginTop:150,
  },
  connt:{
    marginLeft :20,
    flexDirection: 'row',
      justifyContent: 'flex-start',
  },
  tet:{
    marginLeft :20,
  }

});

export default styles;