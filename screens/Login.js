import React, {Component} from 'react';
import {AppRegistry,Text,View,Button,TextInput,Image,TouchableHighlight,Linking,ImageBackground,Alert,FlatList,ListView,AsyncStorage  } from 'react-native';
import{CheckBox } from 'native-base'
import urlBase from '../constants/urls'


//Importando hoja de estilos
import styles from '../styles/styles';

import HomeScreen from '../screens/HomeScreen';
class Login extends Component{
	constructor(props) {
	  super(props);
	   this.state = {
	   	  data: '',
        loading:false,
        url: urlBase+'Usuarios/LogIn',
        usuario:''

      };
	 
	}

checklogin()
{
    

  
	const{username,password} = this.state	

this.setState({loading:true})
      fetch(this.state.url, {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }, body: JSON.stringify({
    usuario: username,
    contrasenia:password
  }),
}).then(res => res.json()).
then(res =>
  {
    //console.log(res);
    if(res.resultado == 0)
    {
      Alert.alert('Error','Usuario o contraseña incorrecto',
      [{
        text:'Okay'
      }]
      )
    }
    else {
      AsyncStorage.setItem('jwt', res.datos)
      const usuario  = res.datos.usuario;
      console.log('usuario');
      console.log(usuario);
          alert(`Bienvenido! .`+res.datos.usuario);
          this.setState({usuario:usuario});
          // Redirect to home screen
          this.props.navigation.navigate('Home',{usuario:this.state.usuario})
    }
  });

/*
	if(username == 'adimn' || password == 'admin')
	{
		this.props.navigation.navigate('Home')
	}
	else {
		Alert.alert('Error','Usuario o contraseña incorrecto',
			[{
				text:'Okay'
			}]
			)
	}*/
}

	render(){

const {navigate} = this.props.navigation;
		return(
			<ImageBackground style={styles.imagencontain} source={require('../assets/images/header.jpeg')}>
			<View style = {styles.container}>
			
			<View style = {styles.MainContainer}>

				<View  style = {styles.MainContainerlogitex} >
					
					<Text style={styles.LoginText}>Inicio de Sesion </Text>
				</View>
		<View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={require('../assets/images/usericon.jpg')}/>
          <TextInput style={styles.inputs}
              placeholder="Usuario"
              onChangeText={text => this.setState({username:text})}
              underlineColorAndroid='transparent'
              />
        </View>
			<View style={styles.inputContainer2}>
          <Image style={styles.inputIcon} source={require('../assets/images/key.jpg')}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={text => this.setState({password:text})}
              />
        </View>
        <View style={styles.check}>
        <CheckBox  color='white' />
        <Text style={styles.checkText}>Recordarme</Text>
        
    </View>

        <View style = {styles.MainContainerbuton} > 
			<TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.checklogin() }>
          		<Text style={styles.loginText}>Login</Text>
    		</TouchableHighlight>
    		
    </View>
   
    <View  style = {styles.MainContainerlink}> 
    		<Text style={styles.passText} onPress={() => Linking.openURL('http://google.com')}>¿Ha olvidado su contraseña?</Text>
    </View>
    
				</View>
					<View >
			
			</View>
			</View>
		
			</ImageBackground>
			
		);
	}
}

export default Login;