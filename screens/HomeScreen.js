import React, {Fragment} from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Left, Right, Body, Icon } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';



class HomeScreen extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      loading:false,
      url:'http://192.168.0.39:57885/RecoleccionDatos/Usuarios/CerrarSesion',
    };

  }
  componentDidMount(){
    console.log('datos en home');
  }
  logen(){
    fetch(this.state.url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }, body: JSON.stringify({
        usuario: username,
        contrasenia:password
      }),
    }).then(res => res.json()).
    then(res =>
    {
      console.log(res);
      if(res.resultado == 0)
      {
        Alert.alert('Error','Usuario o contraseña incorrecto',
          [{
            text:'Okay'
          }]
          )
      }
      else {
        AsyncStorage.setItem('jwt', res.datos)
        alert(`Bienvenido! .`+res.datos.usuario)
// Redirect to home screen
this.props.navigation.navigate('Home')
}
});

  }
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: () => <Ionicons name="md-home" size={24} />
  };
  render() {
    return (
      <Container>
      <Header>
      <Left>
      <Button transparent>
      <Icon name='menu' onPress={() => this.props.navigation.openDrawer()}
      />
      </Button>
      </Left>
      <Body>

      </Body>
      <Right />
      </Header>
      <Content>

      </Content>
      <Footer>
      <FooterTab>
      <Button full>

      </Button>
      </FooterTab>
      </Footer>
      </Container>
      );
  }
}

export default HomeScreen;