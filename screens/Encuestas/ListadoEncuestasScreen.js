import React, {Fragment,Component} from 'react';
import * as WebBrowser from 'expo-web-browser';
import axios from 'axios';
import { 
  StyleSheet, 
  Text, 
  View,
  SafeAreaView,
  ScrollView,
  StatusBar ,
  Button
} from 'react-native';
  import { 
  Container, 
  Content, 
  Header, 
  Left, 
  Body, 
  Icon } from 'native-base';

import Ionicons from 'react-native-vector-icons/Ionicons';

  class ListadoEncuestasScreen extends React.Component{
  	constructor(props) {
  	  super(props);

  	  this.state = {
        loading:false,
        data:[],
        url:'http://localhost:57885/RecoleccionDatos/Herramientas'
      };
  	}


    getData(){
     
      fetch("http://localhost:57885/RecoleccionDatos/Herramientas")
      .then(response => {
        console.log(response);
        response.json();
      })
      .then(responseJson => {
        console.log(responseJson);
      })
      .catch(error => {
        console.warn('There has been a problem with your fetch operation: ' + error.message);
        
      });

    };


    static navigationOptions = {
      drawerLabel: 'Proyectos',
      drawerIcon: () => <Ionicons name="md-clipboard" size={24} />
    };
  	render() {
  		return (
  			<Container>
          <Header>
            <Left>
              <Icon name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
            </Left>
          </Header>
          <Content contentContinerStyle={
          {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'          }
          }>
            <Text>Listado de encuestas</Text>
            <Button title="presione" onPress={() => this.getData() }/> 
          </Content>
        </Container>
  		);
  	}
  }

  export default ListadoEncuestasScreen;