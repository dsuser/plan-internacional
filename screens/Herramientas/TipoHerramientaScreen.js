import React, {Component} from 'react';
import { View, TouchableOpacity, Text, FlatList, StyleSheet} from "react-native";
import { Colors } from '../../constants/Colors';
import Icon from "react-native-vector-icons/MaterialIcons";
import Accordian from '../../components/Accordian';

class TipoHerramientaScreen extends Component{
static navigationOptions = {
      drawerLabel: 'Herramientas',
      
    };
    constructor(props) {
    super(props);
    this.state = {
      menu :[
        { 
          title: 'Non Veg Biryanis', 
          data: [
            {key:'Chicken Biryani', value:'false'},
            {key:'Mutton Biryani', value:'false'},
            {key:'Prawns Biryani', value:'false'},
          ] 
        },
        { 
          title: 'Pizzas',
          data: [
            {key:'Chicken Dominator', value:'false'},
            {key:'Peri Peri Chicken', value:'false'},
            {key:'Indie Tandoori Paneer', value:'false'},
            {key:'Veg Extraveganza', value:'false'}
          ]
        },
        { 
         title: 'Drinks',
         data: [
           {key:'Cocktail',value:'false'},
           {key:'Mocktail',value:'false'},
           {key:'Lemon Soda',value:'false'},
           {key:'Orange Soda', value:'false'}
          ]
        },
        { 
          title: 'Deserts',
          data: [
            {key:'Choco Lava Cake', value:'false'},
            {key:'Gulabjamun', value:'false'},
            {key:'Kalajamun', value:'false'},
            {key:'Jalebi', value:'false'}
          ]
        },
      ]
     }
  }
  
  render() {

    return (
       <View style={styles.container}>
        { this.renderAccordians() }
      </View>
    )
  }

  onClick=(index)=>{
    const temp = this.state.data.slice()
    temp[index].value = !temp[index].value
    this.setState({data: temp})
  }

  toggleExpand=()=>{
    this.setState({expanded : !this.state.expanded})
  }

  renderAccordians=()=> {
    const items = [];
    for (item of this.state.menu) {
        items.push(
            <Accordian 
                title = {item.title}
                data = {item.data}
            />
        );
    }
    return items;
}

}

const styles = StyleSheet.create({
  container: {
   flex:1,
   paddingTop:100,
   backgroundColor:Colors.PRIMARY,
   
  }
});

export default TipoHerramientaScreen;