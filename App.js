import React, {Fragment} from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Image } from 'react-native';


// Elementos del navegador
import { createAppContainer } from 'react-navigation';
//Estilos
import styles from './styles/styles';
//navegador de elementos
import DrawerNavigator from './navigation/navigator';

export default class App extends React.Component{
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <AppContainer  />
    );
  }
}


const AppContainer = createAppContainer(DrawerNavigator);

