import React, {Fragment} from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Image,
  TouchableHighlight ,
TouchableOpacity} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Left,
  Right, 
  Body,
   Accordion,
  Button, 
  Icon,
  List,
  ListItem,
  Item,Thumbnail, Separator  } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';

//Pantallas
import HomeScreen from '../screens/HomeScreen';
import ListadoEncuestasScreen from '../screens/Encuestas/ListadoEncuestasScreen';
import TipoHerramientasScreen from '../screens/Herramientas/TipoHerramientaScreen';
import Login from'../screens/Login';
import styles from '../styles/styles';
import Slider from '../components/Slider';
//Navegador personalizado con los elementos que contendra la aplicacion

const sliderbar = () =>
{
  return(
      <Slider/>
    )
}


const DrawerNavigator = createDrawerNavigator(
  {
    Home: HomeScreen,
    ListadoEncuestas: ListadoEncuestasScreen,
    TipoHerramientas: TipoHerramientasScreen,
    Login : Login 
  },
  {
    initialRouteName: 'Home',
    contentComponent: ({navigation}) => <Slider navigation ={navigation}/>,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
  }
);

export default DrawerNavigator;